<?php


use App\Models\News;
use App\Repositories\Contracts\NewsRepositoryContract;
use App\Repositories\NewsRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

test('get_all_news', function (Collection $allCategories, array $allCategoriesResponse) {
    $news = Mockery::mock(News::class);
    $builder = Mockery::mock(Builder::class);
    $news->shouldReceive('query')->andReturn($builder);
    $builder->shouldReceive('paginate')->andReturn($allCategories);
    $this->app->instance(NewsRepositoryContract::class, new NewsRepository($news));
    $response = $this->get('/api/news')->json();
//    dd($response, $allCategoriesResponse);
    expect($response)->toBe($allCategoriesResponse);
})
    ->with('all_news');


test('get_news_by_id', function (int $id, Collection $news, array $newsByIdResponse) {
    $builder = Mockery::mock(Builder::class);
    $builder->shouldReceive('find')->andReturn($news);
    $newsModel = Mockery::mock(News::class);
    $newsModel->shouldReceive('query')->andReturn($builder);
    $this->app->instance(NewsRepositoryContract::class, new NewsRepository($newsModel));
    $response = $this->get('/api/news/'.$id)->json();
    expect($response)->toBe($newsByIdResponse);
})
    ->with('news_by_id');


test('create_news', function (array $requestCreateNews, Collection $newsCreateResult, array $newsCreateResponse) {
    $builder = Mockery::mock(Builder::class);
    $builder->shouldReceive('create')->andReturn($newsCreateResult);
    $newsModel = Mockery::mock(News::class);
    $newsModel->shouldReceive('query')->andReturn($builder);
    $this->app->instance(NewsRepositoryContract::class, new NewsRepository($newsModel));
    $response = $this->post('/api/news', $requestCreateNews)->json();
    expect($response)->toBe($newsCreateResponse);
})
    ->with('news_create');


test('update_news', function (int $id, array $requestUpdateNews, Collection $newsUpdateResult, array $newsUpdateResponse) {
    $builder = Mockery::mock(Builder::class);
    $newsModel = Mockery::mock(News::class);
    $builder->shouldReceive('find')->andReturn($newsModel);
    $newsModel->shouldReceive('toArray')->andReturn($newsUpdateResult->toArray());
    $newsModel->shouldReceive('query')->andReturn($builder);
    $newsModel->shouldReceive('update')->andReturn(true);
    $this->app->instance(NewsRepositoryContract::class, new NewsRepository($newsModel));
    $response = $this->put('/api/news/'.$id, $requestUpdateNews)->json();
    expect($response)->toBe($newsUpdateResponse);
})
    ->with('news_update');


test('delete_news', function (int $id, News $newsFindResult) {
    $builder = Mockery::mock(Builder::class);
    $builder->shouldReceive('find')->andReturn($newsFindResult);
    $newsModel = Mockery::mock(News::class);
    $newsModel->shouldReceive('query')->andReturn($builder);
    $newsModel->shouldReceive('delete')->andReturn(true);
    $this->app->instance(NewsRepositoryContract::class, new NewsRepository($newsModel));
    $response = $this->delete('/api/news/'.$id);
    expect($response->status())->toBe(204);
})
    ->with('news_delete');


