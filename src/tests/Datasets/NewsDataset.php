<?php
use App\Models\News;

dataset('all_news', function () {
    $now = now()->toDateTimeString();
    return [
        [
            // mocking model
            collect([
                [
                    'id' => 1,
                    'title' => 'example',
                    'url' => 'example',
                    'short_description' => 'example',
                    'full_description' => 'example',
                    'is_show' => false,
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'id' => 2,
                    'title' => 'example',
                    'url' => 'example',
                    'short_description' => 'example',
                    'full_description' => 'example',
                    'is_show' => false,
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'id' => 3,
                    'title' => 'example',
                    'url' => 'example',
                    'short_description' => 'example',
                    'full_description' => 'example',
                    'is_show' => false,
                    'created_at' => $now,
                    'updated_at' => $now,
                ]
            ]),
            // response
            [
                [
                    'id' => 1,
                    'title' => 'example',
                    'url' => 'example',
                    'short_description' => 'example',
                    'full_description' => 'example',
                    'is_show' => false,
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'id' => 2,
                    'title' => 'example',
                    'url' => 'example',
                    'short_description' => 'example',
                    'full_description' => 'example',
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'id' => 3,
                    'title' => 'example',
                    'url' => 'example',
                    'short_description' => 'example',
                    'full_description' => 'example',
                    'created_at' => $now,
                    'updated_at' => $now,
                ]
            ]
        ]
    ];
});

dataset('news_by_id', function () {
    $now = now()->toDateTimeString();
    return [
        [
            //id
            1,
            // mocking model
            collect([
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
                'created_at' => $now,
                'updated_at' => $now,
            ]),
            // response
            [
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
                'created_at' => $now,
                'updated_at' => $now,
            ]
        ]
    ];
});

dataset('news_create', function () {
    $now = now()->toDateTimeString();

    return [
        [
            //request for create news
            [
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false
            ],
            // mocking model
            collect([
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
                'created_at' => $now,
                'updated_at' => $now,
            ]),
            // response
            [
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
                'created_at' => $now,
                'updated_at' => $now,
            ]
        ]
    ];
});

dataset('news_update', function () {
    $now = now()->toDateTimeString();

    return [
        [
            // news id
            1,
            //request for update news
            [
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false
            ],
            // mocking model
            collect([
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
            ]),
            // response
            [
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
            ]
        ]
    ];
});

dataset('news_delete', function () {
    $now = now()->toDateTimeString();

    return [
        [
            // news id
            1,
            // mocking model
            new News([
                'id' => 1,
                'title' => 'example',
                'url' => 'example',
                'short_description' => 'example',
                'full_description' => 'example',
                'is_show' => false,
                'created_at' => $now,
                'updated_at' => $now,
            ])
        ]
    ];
});
