<?php

namespace Database\Factories;

use App\Constants\DBConstants\NewsTableConstants;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\News>
 */
class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            NewsTableConstants::FIELD_TITLE => fake()->text(rand(10,100)),
            NewsTableConstants::FIELD_URL => fake()->url,
            NewsTableConstants::FIELD_SHORT_DESCRIPTION => fake()->text(rand(50,200)),
            NewsTableConstants::FIELD_FULL_DESCRIPTION => fake()->text(rand(200,4000)),
            NewsTableConstants::FIELD_IS_SHOW => fake()->boolean,
        ];
    }
}
