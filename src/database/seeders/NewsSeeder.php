<?php

namespace Database\Seeders;

use App\Constants\DBConstants\NewsTableConstants;
use App\Models\News;
use App\Repositories\Contracts\NewsRepositoryContract;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    public function __construct(
        protected NewsRepositoryContract $newsRepository
    ) {
    }
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        News::factory()->count(50)->create();
    }
}
