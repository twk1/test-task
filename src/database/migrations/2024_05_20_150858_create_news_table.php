<?php

use App\Constants\DBConstants\NewsTableConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(NewsTableConstants::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(NewsTableConstants::FIELD_TITLE);
            $table->string(NewsTableConstants::FIELD_URL);
            $table->string(NewsTableConstants::FIELD_SHORT_DESCRIPTION);
            $table->text(NewsTableConstants::FIELD_FULL_DESCRIPTION);
            $table->boolean(NewsTableConstants::FIELD_IS_SHOW);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists(NewsTableConstants::TABLE_NAME);
    }
};
