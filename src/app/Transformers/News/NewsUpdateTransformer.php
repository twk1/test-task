<?php

namespace App\Transformers\News;

use App\Constants\DBConstants\NewsTableConstants;
use App\Dtos\News\NewsCreateDto;
use App\Dtos\News\NewsUpdateDto;

class NewsUpdateTransformer
{
    public function transform(NewsUpdateDto $newsUpdateDto): array
    {
        return [
            NewsTableConstants::FIELD_TITLE => $newsUpdateDto->getTitle(),
            NewsTableConstants::FIELD_URL => $newsUpdateDto->getUrl(),
            NewsTableConstants::FIELD_SHORT_DESCRIPTION => $newsUpdateDto->getShortDescription(),
            NewsTableConstants::FIELD_FULL_DESCRIPTION => $newsUpdateDto->getFullDescription(),
            NewsTableConstants::FIELD_IS_SHOW => $newsUpdateDto->getIsShow()
        ];
    }
}
