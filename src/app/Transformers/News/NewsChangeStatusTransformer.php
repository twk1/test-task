<?php

namespace App\Transformers\News;

use App\Constants\DBConstants\NewsTableConstants;
use App\Dtos\News\NewsChangeStatusDto;
use App\Dtos\News\NewsUpdateDto;

class NewsChangeStatusTransformer
{
    public function transform(NewsChangeStatusDto $newsChangeStatusDto): array
    {
        return [
            NewsTableConstants::FIELD_IS_SHOW => (int) $newsChangeStatusDto->getIsShow()
        ];
    }
}
