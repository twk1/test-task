<?php

namespace App\Transformers\News;

use App\Constants\DBConstants\NewsTableConstants;
use App\Dtos\News\NewsCreateDto;

class NewsCreateTransformer
{
    public function transform(NewsCreateDto $newsCreateDto): array
    {
        return [
            NewsTableConstants::FIELD_TITLE => $newsCreateDto->getTitle(),
            NewsTableConstants::FIELD_URL => $newsCreateDto->getUrl(),
            NewsTableConstants::FIELD_SHORT_DESCRIPTION => $newsCreateDto->getShortDescription(),
            NewsTableConstants::FIELD_FULL_DESCRIPTION => $newsCreateDto->getFullDescription(),
            NewsTableConstants::FIELD_IS_SHOW => $newsCreateDto->getIsShow()
        ];
    }
}
