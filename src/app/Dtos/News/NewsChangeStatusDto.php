<?php

namespace App\Dtos\News;

readonly class NewsChangeStatusDto
{
    public function __construct(
        private string $isShow
    )
    {
    }

    /**
     * @return string
     */
    public function getIsShow(): string
    {
        return $this->isShow;
    }
}
