<?php

namespace App\Dtos\News;

readonly class NewsUpdateDto
{
    public function __construct(
        private string $title,
        private string $url,
        private string $shortDescription,
        private string $fullDescription,
        private string $isShow
    )
    {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getFullDescription(): string
    {
        return $this->fullDescription;
    }

    /**
     * @return string
     */
    public function getIsShow(): string
    {
        return $this->isShow;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
