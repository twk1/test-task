<?php

namespace App\Models;

use App\Constants\DBConstants\NewsTableConstants;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class News extends Model
{
    use HasFactory;

    protected $table = NewsTableConstants::TABLE_NAME;

    protected $fillable = [
        NewsTableConstants::FIELD_TITLE,
        NewsTableConstants::FIELD_FULL_DESCRIPTION,
        NewsTableConstants::FIELD_SHORT_DESCRIPTION,
        NewsTableConstants::FIELD_URL,
        NewsTableConstants::FIELD_IS_SHOW
    ];

    protected $casts = [
        NewsTableConstants::FIELD_IS_SHOW => 'boolean'
    ];
}
