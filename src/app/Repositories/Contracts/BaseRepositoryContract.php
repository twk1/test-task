<?php

namespace App\Repositories\Contracts;

interface BaseRepositoryContract
{
    public function getPaginated(): array;
    public function find(int $id): array;
    public function create(array $data): array;
    public function update(int $id, array $data): array;
    public function delete(int $id): bool;
}
