<?php

namespace App\Repositories;

use App\Exceptions\NotFoundException;
use App\Repositories\Contracts\BaseRepositoryContract;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements BaseRepositoryContract
{
    public function __construct(
        private readonly Model $model
    )
    {
    }

    /**
     * @return array
     */
    public function getPaginated($isShow = null): array
    {
        return $this->model->query()->when($isShow !== null, function ($q) use ($isShow) {
            return $q->where('is_show', $isShow);
        })->paginate()->toArray();
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundException
     */
    public function find(int $id): array
    {
        $entity = $this->model->query()->find($id);
        return $entity
            ? $entity->toArray()
            : throw new NotFoundException('Entity not found');
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        return $this->model->query()->create($data)->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return array
     * @throws NotFoundException
     */
    public function update(int $id, array $data): array
    {
        $entity = $this->model->query()->find($id);

        $entity
            ? $entity->update($data)
            : throw new NotFoundException('Entity not found');

        return $entity->toArray();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $entity = $this->model->query()->find($id);

        return $entity ? $entity->delete() : true;
    }

}
