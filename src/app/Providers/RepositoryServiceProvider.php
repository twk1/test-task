<?php

namespace App\Providers;

use App\Models\News;
use App\Repositories\Contracts\NewsRepositoryContract;
use App\Repositories\NewsRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(NewsRepositoryContract::class, function (){
            return new NewsRepository(new News());
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
