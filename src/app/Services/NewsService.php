<?php

namespace App\Services;

use App\Dtos\News\NewsChangeStatusDto;
use App\Dtos\News\NewsCreateDto;
use App\Dtos\News\NewsUpdateDto;
use App\Repositories\Contracts\NewsRepositoryContract;
use App\Transformers\News\NewsChangeStatusTransformer;
use App\Transformers\News\NewsCreateTransformer;
use App\Transformers\News\NewsUpdateTransformer;

readonly class NewsService
{
    public function __construct(
        private NewsRepositoryContract $newsRepository,
        private NewsCreateTransformer $newsCreateTransformer,
        private NewsUpdateTransformer $newsUpdateTransformer,
        private NewsChangeStatusTransformer $newsChangeStatusTransformer,
    )
    {
    }

    /**
     * @return array
     */
    public function getAllActive(): array
    {
        return $this->newsRepository->getPaginated(true);
    }

    /**
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        return $this->newsRepository->find($id);
    }

    /**
     * @param int $id
     * @param NewsUpdateDto $newsUpdateDto
     * @return array
     */
    public function update(int $id, NewsUpdateDto $newsUpdateDto): array
    {
        return $this->newsRepository->update($id, $this->newsUpdateTransformer->transform($newsUpdateDto));
    }

    /**
     * @param int $id
     * @param NewsChangeStatusDto $newsChangeStatusDto
     * @return array
     */
    public function changeStatus(int $id, NewsChangeStatusDto $newsChangeStatusDto): array
    {
        return $this->newsRepository->update($id, $this->newsChangeStatusTransformer->transform($newsChangeStatusDto));
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->newsRepository->delete($id);
    }

    /**
     * @param NewsCreateDto $newsCreateDto
     * @return array
     */
    public function create(NewsCreateDto $newsCreateDto): array
    {
        return $this->newsRepository->create($this->newsCreateTransformer->transform($newsCreateDto));
    }
}
