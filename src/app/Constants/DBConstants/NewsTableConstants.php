<?php

namespace App\Constants\DBConstants;

class NewsTableConstants
{
    public const TABLE_NAME = 'news';
    public const FIELD_TITLE = 'title';
    public const FIELD_URL = 'url';
    public const FIELD_SHORT_DESCRIPTION = 'short_description';
    public const FIELD_FULL_DESCRIPTION = 'full_description';
    public const FIELD_IS_SHOW = 'is_show';
}
