<?php

namespace App\Constants\RequestConstants\News;

abstract class NewsUpdateRequestConstants
{
    public const TITLE = 'title';
    public const URL = 'url';
    public const SHORT_DESCRIPTION = 'short_description';
    public const FULL_DESCRIPTION = 'full_description';
    public const IS_SHOW = 'is_show';
}
