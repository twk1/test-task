<?php

namespace App\Http\Controllers;

use App\Constants\RequestConstants\News\NewsCreateRequestConstants;
use App\Constants\RequestConstants\News\NewsUpdateRequestConstants;
use App\Dtos\News\NewsChangeStatusDto;
use App\Dtos\News\NewsCreateDto;
use App\Dtos\News\NewsUpdateDto;
use App\Http\Requests\News\NewsChangeStatusRequest;
use App\Http\Requests\News\NewsCreateRequest;
use App\Http\Requests\News\NewsUpdateRequest;
use App\Services\NewsService;
use Illuminate\Http\JsonResponse;

class NewsController extends Controller
{
    public function __construct(
        private readonly NewsService $newsService
    )
    {
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->newsService->getAllActive());
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return response()->json($this->newsService->getById($id));
    }

    /**
     * @param int $id
     * @param NewsUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, NewsUpdateRequest $request): JsonResponse
    {
        $newsUpdateDto = new NewsUpdateDto(
            title: $request->validated(NewsCreateRequestConstants::TITLE),
            url: $request->validated(NewsCreateRequestConstants::URL),
            shortDescription: $request->validated(NewsCreateRequestConstants::SHORT_DESCRIPTION),
            fullDescription: $request->validated(NewsCreateRequestConstants::FULL_DESCRIPTION),
            isShow: $request->validated(NewsCreateRequestConstants::IS_SHOW),
        );

        return response()->json($this->newsService->update($id, $newsUpdateDto));
    }

    /**
     * @param NewsCreateRequest $request
     * @return JsonResponse
     */
    public function store(NewsCreateRequest $request): JsonResponse
    {
        $newsCreateDto = new NewsCreateDto(
            title: $request->validated(NewsCreateRequestConstants::TITLE),
            url: $request->validated(NewsCreateRequestConstants::URL),
            shortDescription: $request->validated(NewsCreateRequestConstants::SHORT_DESCRIPTION),
            fullDescription: $request->validated(NewsCreateRequestConstants::FULL_DESCRIPTION),
            isShow: $request->validated(NewsCreateRequestConstants::IS_SHOW),
        );

        return response()->json($this->newsService->create($newsCreateDto));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return response()->json($this->newsService->delete($id), 204);
    }

    /**
     * @param int $id
     * @param NewsChangeStatusRequest $request
     * @return JsonResponse
     */
    public function changeStatus(int $id, NewsChangeStatusRequest $request): JsonResponse
    {
        $newsChangeStatusDto = new NewsChangeStatusDto(
            isShow: $request->validated(NewsCreateRequestConstants::IS_SHOW),
        );

        return response()->json($this->newsService->changeStatus($id, $newsChangeStatusDto));
    }
}
