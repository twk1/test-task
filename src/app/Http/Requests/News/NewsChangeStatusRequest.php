<?php

namespace App\Http\Requests\News;

use App\Constants\RequestConstants\News\NewsChangeStatusRequestConstants;
use App\Constants\RequestConstants\News\NewsCreateRequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class NewsChangeStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            NewsChangeStatusRequestConstants::IS_SHOW => 'required|boolean'
        ];
    }
}
