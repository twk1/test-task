<?php

namespace App\Http\Requests\News;

use App\Constants\RequestConstants\News\NewsCreateRequestConstants;
use App\Constants\RequestConstants\News\NewsUpdateRequestConstants;
use Illuminate\Foundation\Http\FormRequest;

class NewsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            NewsUpdateRequestConstants::TITLE => 'required|string',
            NewsUpdateRequestConstants::URL => 'required|string',
            NewsUpdateRequestConstants::SHORT_DESCRIPTION => 'required|string',
            NewsUpdateRequestConstants::FULL_DESCRIPTION => 'required|string',
            NewsUpdateRequestConstants::IS_SHOW => 'required|boolean',
        ];
    }
}
